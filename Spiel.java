import java.util.Random;
/**
 * Beschreiben Sie hier die Klasse Spiel.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spiel
{
    public int wurf1;
    public int wurf2;
    public int Runde;
    public int maxRunde;
    private int SpielerAnzahl;
    private int AmZug;
    public Spieler aktiverSpieler;
    public Spieler[] SpielerListe;
    public boolean istGewonnen = false;
    public Feld[] Spielbrett = new Feld[40];
    Random wuerfel = new Random();

    /**
     * Konstruktor für Objekte der Klasse Spiel
     */
    public Spiel(int max, int spieler)
    {
        Runde = 0;
        maxRunde = max;
        SpielerAnzahl = spieler;
        SpielerListe = new Spieler[SpielerAnzahl];
        SpielerErstellen();
        InitialiseBoard();
        AmZug = 1;
        Play();
    }
    
    private void Play(){
        while (!istGewonnen && Runde < maxRunde){
            for (int i = 0; i < SpielerAnzahl; i++){
                aktiverSpieler = SpielerListe[i];
                
                wurf1 = 1 + wuerfel.nextInt(6);
                wurf2 = 1 + wuerfel.nextInt(6);
                
                if (aktiverSpieler.rundenGefangen == 0){
                    aktiverSpieler.istAufFeld += wurf1 + wurf2;
                    Spielbrett[aktiverSpieler.istAufFeld].aktiviert();
                }
                else {
                    if (wurf1 == wurf2)
                        aktiverSpieler.rundenGefangen = 0;
                        
                }
            }
        } 
    }
    
    private void SpielerErstellen(){
        for (int i = 0; i < SpielerListe.length; i++){
            SpielerListe[0] = new Spieler();
        }
    }
    
    private void InitialiseBoard(){
        Spielbrett[0] = new Spezial("Los", 200);
        Spielbrett[1] = new Straße();
        Spielbrett[10] = new Spezial("Gefängnis", 0);
        Spielbrett[30] = new Spezial("Gehe in das Gefängnis", 0);
    }
}
