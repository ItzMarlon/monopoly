import java.util.Random;
/**
 * Beschreiben Sie hier die Klasse Werk.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Werk extends Feld
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int x;

    /**
     * Konstruktor für Objekte der Klasse Werk
     */
    public Werk()
    {
        // Instanzvariable initialisieren
        x = 0;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public int MieteBerechnen(Spieler mieter)
    {
        int miete;
        Random wuerfel = new Random();
       
        int wurf1 = 1 + wuerfel.nextInt(6);
        int wurf2 = 1 + wuerfel.nextInt(6);
        
        miete = wurf1 + wurf2;
        if (mieter.anzahlWerke == 1) {
            miete = miete * 4;
        }
        else if (mieter.anzahlWerke == 2){
            miete = miete * 10;
        }
        else {
            miete = 0;
        }
                
        return miete;
    }
}
