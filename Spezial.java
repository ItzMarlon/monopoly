
/**
 * Beschreiben Sie hier die Klasse Spezial.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spezial extends Feld
{
    public String name;
    public int Geld;
    /**
     * Konstruktor für Objekte der Klasse Spezial
     */
    public Spezial(String Name, int money) 
    {
      name = Name;
      Geld = money;
    }
    
    public void aktiviert(Spieler player){
        if (name.equals("Gehe in das Gefängnis")){
            player.istAufFeld = 10;
        }
        else {
            player.Kontostand += Geld;
        }
    }
}
